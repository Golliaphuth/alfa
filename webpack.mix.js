const mix = require('laravel-mix');

mix
    .js('resources/js/app.js', 'public/js')
    .sass('resources/scss/theme.scss', 'public/css').sourceMaps()
    .sass('resources/scss/style.scss', 'public/css').sourceMaps()
;
